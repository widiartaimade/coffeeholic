package com.latihan.widiarta.cofeeholics;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class DetailCoffee extends AppCompatActivity {

    private Context context;
    public static final String EXTRA_NAMA = "Nama ";
    public static final String EXTRA_DEFINISI = "Definisi";
    public static final String EXTRA_KARAKTERISTIK = "Karakteristik";
    public static final String EXTRA_FOTO = "KOSONG";


    TextView tvDataReceived;
    private ArrayList<Coffee> listCoffee;
    private ArrayList<Coffee> getListPresident() {
        return listCoffee;
    }

    private void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_coffee);

        ImageView imgPhoto;
        TextView tvNama, tvDefinisi, tvKarakteristik;

        tvDataReceived = findViewById(R.id.tv_data_received);
        tvNama = findViewById(R.id.tv_item_nama);
        tvDefinisi = findViewById(R.id.tv_item_definisi);
        tvKarakteristik = findViewById(R.id.tv_item_karakteristik);
        imgPhoto = findViewById(R.id.img_item_photo);

        String nama = getIntent().getStringExtra(EXTRA_NAMA);
        String definisi = getIntent().getStringExtra(EXTRA_DEFINISI);
        String karakteristik = getIntent().getStringExtra(EXTRA_KARAKTERISTIK);
        String foto = getIntent().getStringExtra(EXTRA_FOTO);

        setActionBarTitle("Detail Coffee");
        tvNama.setText(nama);
        tvDefinisi.setText(definisi);
        tvKarakteristik.setText(karakteristik);

        Glide.with(DetailCoffee.this)
                .load(foto)
                .apply(new RequestOptions().override(350, 550))
                .into(imgPhoto);

    }


}
