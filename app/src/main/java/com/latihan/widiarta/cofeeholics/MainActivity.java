package com.latihan.widiarta.cofeeholics;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvCategory;
    private ArrayList<Coffee> list = new ArrayList<>();

    private void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvCategory = findViewById(R.id.rv_category);
        rvCategory.setHasFixedSize(true);

        list.addAll(CoffeeData.getListData());
        showRecyclerList();
    }

    private void showRecyclerList(){
        rvCategory.setLayoutManager(new LinearLayoutManager(this));
        ListCoffeeAdapter listPresidentAdapter = new ListCoffeeAdapter(this);
        listPresidentAdapter.setListCoffee(list);
        rvCategory.setAdapter(listPresidentAdapter);
        setActionBarTitle("Coffee Holic");
        ItemClickSupport.addTo(rvCategory).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                //showSelectedPresident(list.get(position));
                ShowDetailCoffee(list.get(position));
            }
        });
    }

    private void showSelectedPresident(Coffee coffee){
        Toast.makeText(this, "Kamu memilih "+coffee.getName(), Toast.LENGTH_SHORT).show();
    }

    private void ShowDetailCoffee(Coffee coffee){

        Intent moveWithDataIntent = new Intent(MainActivity.this, DetailCoffee.class);
        moveWithDataIntent.putExtra(DetailCoffee.EXTRA_NAMA, coffee.getName());
        moveWithDataIntent.putExtra(DetailCoffee.EXTRA_DEFINISI, coffee.getDefinisi());
        moveWithDataIntent.putExtra(DetailCoffee.EXTRA_KARAKTERISTIK, coffee.getKarakteristik());
        moveWithDataIntent.putExtra(DetailCoffee.EXTRA_FOTO, coffee.getPhoto());
        startActivity(moveWithDataIntent);

    }
}
