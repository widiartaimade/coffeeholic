package com.latihan.widiarta.cofeeholics;

public class Coffee {

    private String name, definisi, karakteristik, photo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDefinisi() {
        return definisi;
    }

    public void setDefinisi(String definisi) {
        this.definisi = definisi;
    }

    public String getKarakteristik() {
        return karakteristik;
    }

    public void setKarakteristik(String karakteristik) {
        this.karakteristik = karakteristik;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }



}
