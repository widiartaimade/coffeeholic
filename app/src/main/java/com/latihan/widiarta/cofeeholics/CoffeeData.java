package com.latihan.widiarta.cofeeholics;

import java.util.ArrayList;

public class CoffeeData {

    public static String[][] data = new String[][]{
            {"Kopi Arabika", "Kopi Arabika dikenal sebagai kopi tradisional yang memiliki cita rasa terbaik di dunia.", "Kopi ini memiliki ukuran biji lebih kecil dan tersebar hampir di seluruh dunia hingga mampu menguasai 70%-80% pasar dunia. Rasa dan aromanya lebih nikmat dan memiliki kandungan kafein yang lebih rendah sehingga lebih aman untuk dikonsumsi.", "https://kecamatan-lantung.id/asset/dicoding/arabica.jpg"},
            {"Kopi Robusta", "Meski kualitasnya tidak sebaik Arabika, Kopi Robusta memiliki keunggulan dapat tumbuh di daerah sejuk maupun panas, sehingga bisa ditemui di negara-negara tropis maupun subtropis.", "Kopi Robusta memiliki ukuran biji yang besar dan berbentuk oval. Harganya lebih rendah karena rasanya lebih pahit dan sedikit asam, serta kandungan kafeinnya lebih tinggi.","https://kecamatan-lantung.id/asset/dicoding/robusta.jpg"},
            {"Kopi Luwak", "Kopi Luwak sempat booming dan menjadi fenomenal karena dihasilkan melalui proses yang tidak biasa. Biji kopi harus dimakan lebih dahulu oleh luwak, sejenis musang berukuran kecil. Biji kopi yang tidak tercerna sempurna akan dikeluarkan kembali bersama kotoran luwak. Kopi inilah yang diambil dan diproses untuk dijadikan minuman.", "Kopi asli Indonesia ini memiliki rasa yang lembut dan eksotis dengan sentuhan rasa fruity dan sedikit rasa rempah-rempah, sebagai hasil dari proses fermentasi biji kopi oleh luwak. Kadar kafeinnya pun rendah sehingga aman bagi siapa saja. Berbagai keunggulan ini membuat Kopi Luwak dinobatkan sebagai salah satu jenis kopi termahal di dunia.","https://kecamatan-lantung.id/asset/dicoding/coffee.jpg"},
            {"Kopi Sumatra", "Pulau Sumatra memiliki jenis kopi yang sangat banyak dan sebagian besar berasal dari daerah Mandaling.", "Keunggulan Kopi Sumatra adalah teksturnya halus, cita rasanya paling berat dan kompleks di antara jenis kopi lainnya, terasa sedikit asam, berbau tajam dengan aroma rempah khususnya tembakau, dan earthy.","https://kecamatan-lantung.id/asset/dicoding/coffee.jpg"},
            {"Kopi Gayo", "Gayo merupakan penghasil kopi terbesar di Asia, bahkan sudah diekspor dan sangat populer di Amerika Serikat dan Eropa. Kopi Gayo sudah dikembangkan sejak 1908 dan termasuk kopi kelas premium karena diolah dengan sangat teliti.", "Kopi ini memiliki rasa lebih pahit tapi gurih, tingkat keasamannya rendah, dan aromanya sangat tajam sehingga sangat disukai.","https://kecamatan-lantung.id/asset/dicoding/coffee.jpg"},
            {"Kopi Sidikalang", "Kopi yang berasal dari  Kabupaten Dairi, Sumatera Utara, ini dikenal sangat bermutu tinggi. Karena itulah, kopi yang menjadi icon kopi Sumatra ini sangat populer, bahkan mampu bersaing dengan kopi Brazil.", "Kawasan Bukit Barisan merupakan tanah pegunungan yang kaya mineral, sehingga memengaruhi kualitas kopi Sidikalang yang ditanam di kawasan ini.","https://kecamatan-lantung.id/asset/dicoding/coffee.jpg"},
            {"Kopi Arang", "Kopi yang menjadi salah satu kopi terbaik di Jawa ini juga disebut Java Jampit. ", "Kopi beraroma cabai? Itulah keunikan dari Kopi Arang, kopi asli Indonesia yang berasal dari Jember.  Selain aromanya, keunikan Kopi Arang juga terletak pada cara atau proses pembuatannya. Biji kopi disangrai hingga gosong, tetapi rasanya tidak terlalu pahit.","https://kecamatan-lantung.id/asset/dicoding/coffee.jpg"}

    };

    public static ArrayList<Coffee> getListData(){
        Coffee myCoffee = null;
        ArrayList<Coffee> list = new ArrayList<>();
        for (String[] aData : data) {
            myCoffee = new Coffee();
            myCoffee.setName(aData[0]);
            myCoffee.setDefinisi(aData[1]);
            myCoffee.setKarakteristik(aData[2]);
            myCoffee.setPhoto(aData[3]);

            list.add(myCoffee);
        }
        return list;
    }
}
