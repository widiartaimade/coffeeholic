package com.latihan.widiarta.cofeeholics;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class ListCoffeeAdapter extends RecyclerView.Adapter<ListCoffeeAdapter.CategoryViewHolder>  {

    private Context context;

    public ListCoffeeAdapter(Context context) {
        this.context = context;
    }

    public ArrayList<Coffee> getListCoffee() {
        return listCoffee;
    }

    public void setListCoffee(ArrayList<Coffee> listCoffee) {
        this.listCoffee = listCoffee;
    }

    private ArrayList<Coffee> listCoffee;

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemRow = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_coffee, viewGroup, false);
        return new CategoryViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder categoryViewHolder, int position) {
        categoryViewHolder.tvName.setText(getListCoffee().get(position).getName());
        categoryViewHolder.tvDefinisi.setText(getListCoffee().get(position).getDefinisi());
        categoryViewHolder.tvKarakteristik.setText(getListCoffee().get(position).getKarakteristik());

        Glide.with(context)
               .load(getListCoffee().get(position).getPhoto())
              .apply(new RequestOptions().override(55, 55))
              .into(categoryViewHolder.imgPhoto);
    }

    @Override
    public int getItemCount() {
        return getListCoffee().size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvDefinisi;
        TextView tvKarakteristik;
        ImageView imgPhoto;


        CategoryViewHolder(@NonNull View itemView) {
                super(itemView);
                tvName = itemView.findViewById(R.id.tv_item_name);
                tvDefinisi = itemView.findViewById(R.id.tv_item_definisi);
                tvKarakteristik = itemView.findViewById(R.id.tv_item_karakteristik);
                imgPhoto = itemView.findViewById(R.id.img_item_photo);
        }
    }
}
